#!/usr/bin/env python
# coding: utf-8

# In[2]:


import numpy as np
import pandas as pd


# In[3]:


labels = ['a','b','c']
my_list = [10,20,30]
arr = np.array([10,20,30])
d = {'a':10,'b':20,'c':30}


# In[6]:


pd.Series(data = my_list)


# In[7]:


pd.Series(data = my_list, index = labels)


# In[8]:


pd.Series(my_list, labels)


# In[11]:


pd.Series(arr)


# In[12]:


pd.Series(arr, labels)


# In[13]:


pd.Series(d)


# In[14]:


pd.Series(data = labels)


# In[15]:


pd.Series([sum, print, len])


# In[23]:


ser1 = pd.Series([1,2,3,4],index = ['USA','Germany','USSR','Japan'])


# In[24]:


ser1


# In[25]:


ser2 = pd.Series([1,2,5,4],index = ['USA','Germany','Italy','Japan'])


# In[26]:


ser2


# In[27]:


ser1['USA']


# In[28]:


ser1 + ser2


# In[29]:


import numpy as np
import pandas as pd


# In[38]:


from numpy.random import randn
np.random.seed(101)


# In[42]:


df = pd.DataFrame(randn(5,4), index = 'A B C D E'.split(),columns = 'W X Y Z'.split())


# In[40]:


df


# In[43]:


df['W']


# In[45]:


df[['W','Z']]


# In[46]:


df.W


# In[47]:


type(df['W'])


# In[48]:


df['new'] = df['W']+df['Y']


# In[49]:


df


# In[52]:


df.drop('new',axis=1)


# In[53]:


df


# In[54]:


df.drop('new',axis=1, inplace = True)


# In[55]:


df


# In[56]:


df.drop('E',axis = 0)


# In[57]:


df.loc['A']


# In[60]:


df.iloc[2]


# In[61]:


df.loc['B','Y']


# In[62]:


df.loc[['A','B'],['W','Y']]


# In[63]:


df


# In[64]:


df>0


# In[65]:


df[df>0]


# In[66]:


df[df['W']>0]


# In[67]:


df[df['W']>0]['Y']


# In[68]:


df[df['W']>0][['X','Y']]


# In[72]:


df[(df['W']>0) & (df['Y']>1)]


# In[73]:


df


# In[74]:


df.reset_index()


# In[75]:


newind = 'CA NY WY OR CO'.split()


# In[76]:


df['States'] = newind


# In[77]:


df


# In[79]:


df.set_index('States')


# In[80]:


df


# In[81]:


df.set_index('States',inplace = True)


# In[82]:


df


# In[85]:


outside = ['G1','G1','G1','G2','G2','G2']
inside = [1,2,3,1,2,3]
hier_index = list(zip(outside,inside))
hier_index = pd.MultiIndex.from_tuples(hier_index)


# In[86]:


hier_index


# In[87]:


df = pd.DataFrame(np.random.randn(6,2), index = hier_index, columns = ['A','B'])
df


# In[88]:


df.loc['G1']


# In[90]:


df.loc['G1'].loc[1]


# In[91]:


df.index.names


# In[92]:


df.index.names = ['Group', 'Num']


# In[93]:


df


# In[98]:


df.xs('G1')


# In[100]:


df.xs(['G1',1])


# In[101]:


df.xs(1, level = 'Num')


# In[102]:


import numpy as np
import pandas as pd


# In[103]:


df = pd.DataFrame({'A':[1,2,np.nan],
                  'B': [5,np.nan,np.nan],
                  'C':[1,2,3]})


# In[104]:


df


# In[105]:


df.dropna()


# In[106]:


df.dropna(axis=1)


# In[111]:


df.dropna(thresh=2)


# In[112]:


df.fillna(value='FILL VALUE')


# In[114]:


df['A'].fillna(value=df['A'].mean())


# In[115]:


import pandas as pd
data = {'Company':['GOOG','GOOG','MSFT','MSFT','FB','FB'],
       'Person':['Sam','Charlie','Amy','Vanessa','Carl','Sarah'],
       'Sales':[200,120,340,124,243,350]}


# In[116]:


df = pd.DataFrame(data)


# In[117]:


df


# In[118]:


df.groupby('Company')


# In[119]:


by_comp = df.groupby('Company')


# In[122]:


by_comp.mean()


# In[123]:


df.groupby('Company').mean()


# In[124]:


by_comp.std()


# In[125]:


by_comp.min()


# In[126]:


by_comp.max()


# In[127]:


by_comp.count()


# In[128]:


by_comp.describe()


# In[129]:


by_comp.describe().transpose()


# In[130]:


by_comp.describe().transpose()['GOOG']


# In[131]:


import pandas as pd
df1 = pd.DataFrame({'A':['A0','A1','A2','A3'],
                   'B':['B0','B1','B2','B3'],
                   'C':['C0','C1','C2','C3'],
                   'D':['D0','D1','D2','D3']}, index = [0,1,2,3])
df2 = pd.DataFrame({'A':['A4','A5','A6','A7'],
                   'B':['B4','B5','B6','B7'],
                   'C':['C4','C5','C6','C7'],
                   'D':['D4','D5','D6','D7']}, index = [4,5,6,7])
df3 = pd.DataFrame({'A':['A8','A9','A10','A11'],
                   'B':['B8','B9','B10','B11'],
                   'C':['C8','C9','C10','C11'],
                   'D':['D8','D9','D10','D11']}, index = [8,9,10,11])


# In[132]:


df1


# In[133]:


df2


# In[134]:


df3


# In[135]:


pd.concat([df1,df2,df3])


# In[136]:


pd.concat([df1,df2,df3],axis=1)


# In[141]:


left = pd.DataFrame({'key':['K0','K1','K2','K3'],'A':['A0','A1','A2','A3'],'B':['B0','B1','B2','B3']})
right = pd.DataFrame({'key':['K0','K1','K2','K3'],'C':['C0','C1','C2','C3'],'D':['D0','D1','D2','D3']})


# In[142]:


left


# In[143]:


right


# In[144]:


pd.merge(left,right,how='inner',on='key')


# In[149]:


left = pd.DataFrame({'key1':['K0','K0','K1','K2'],'key2':['K0','K1','K0','K1'],'A':['A0','A1','A2','A3'],'B':['B0','B1','B2','B3']})
right = pd.DataFrame({'key1':['K0','K1','K1','K2'],'key2':['K0','K0','K0','K0'],'C':['C0','C1','C2','C3'],'D':['D0','D1','D2','D3']})


# In[151]:


left


# In[152]:


right


# In[150]:


pd.merge(left,right,on=['key1','key2'])


# In[153]:


pd.merge(left,right, how='outer',on=['key1','key2'])


# In[156]:


pd.merge(left,right, how='right',on=['key1','key2'])


# In[157]:


pd.merge(left,right, how='left',on=['key1','key2'])


# In[159]:


left = pd.DataFrame({'A':['A0','A1','A2'],'B':['B0','B2','B3']},index = ['K0','K1','K2'])
right = pd.DataFrame({'C':['C0','C2','C3'],'D':['D0','D2','D3']},index = ['K0','K2','K3'])


# In[160]:


left.join(right)


# In[163]:


left.join(right, how='outer')


# In[164]:


import pandas as pd
df = pd.DataFrame({'col1':[1,2,3,4],'col2':[444,555,666,444],'col3':['abc','def','ghi','xyz']})
df.head()


# In[165]:


df['col2'].unique()


# In[166]:


df['col2'].nunique()


# In[167]:


df['col2'].value_counts()


# In[168]:


newdf = df[(df['col1']>2)&(df['col2']==444)]


# In[169]:


newdf


# In[170]:


def times2(x):
    return x*2


# In[171]:


df['col1'].apply(times2)


# In[172]:


df['col3'].apply(len)


# In[173]:


df['col1'].sum()


# In[174]:


del df['col1']


# In[175]:


df


# In[176]:


df.columns


# In[177]:


df.index


# In[178]:


df


# In[179]:


df.sort_values(by='col2')


# In[180]:


df.isnull()


# In[181]:


df.dropna()


# In[182]:


import numpy as np


# In[185]:


df = pd.DataFrame({'col1':[1,2,3,np.nan],'col2':[np.nan,555,666,444],'col3':['abc','def','ghi','xyz']})
df.head()


# In[186]:


df.fillna('FILL')


# In[187]:


data = {'A':['foo','foo','foo','bar','bar','bar'],
       'B':['one','one','two','two','one','one'],
       'C':['x','y','x','y','x','y'],
       'D':[1,3,2,5,4,1]}
df = pd.DataFrame(data)


# In[188]:


df


# In[189]:


df.pivot_table(values='D',index=['A','B'],columns=['C'])


# In[1]:


import numpy as np
import pandas as pd


# In[6]:


df = pd.read_csv('example')
df


# In[7]:


df.to_csv('example',index = False)


# In[198]:


pip install xlrd


# In[4]:


df = pd.read_excel('Excel_Sample.xlsx',sheet_name='Sheet1')


# In[6]:


df.to_excel('Excel_Sample.xlsx',sheet_name = 'Sheet1')


# In[12]:


pip install lxml


# In[2]:


df = pd.read_html('http://www.fdic.gov/bank/individual/failed/banklist.html')


# In[3]:


df

