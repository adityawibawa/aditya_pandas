#!/usr/bin/env python
# coding: utf-8

# In[1]:


#Import Pandas as pd
import pandas as pd


# In[3]:


#Read banklist.csv Into dataframe called banks
banks = pd.read_csv('banklist.csv')


# In[4]:


banks.head()


# In[5]:


#Show the columns name
banks.columns


# In[14]:


#Show how many states (ST) is represent n the data set
banks['ST'].nunique()


# In[22]:


#Show an array from all state on data set
banks['ST'].to_numpy()


# In[71]:


#Top 5 States with the most failed Bank
grp_ac = banks.groupby('ST')
ac = grp_ac['Bank Name'].count()
ac.sort_values(ascending = False).head()


# In[61]:


# Top 5 Acquiring Insttitution
grp_ac = banks.groupby('Acquiring Institution')
ac = grp_ac['Acquiring Institution'].count()
ac.sort_values(ascending = False).head()


# In[28]:


#Show the Bank with Acquiring Institution State Bank of Texas
banks[banks['Acquiring Institution']=='State Bank of Texas']


# In[94]:


# Show the common city in california for a bank to fail in
a = banks[banks['City']=='Los Angeles']
a.groupby('City').count()


# In[173]:


#Count the Bank Name without 'Bank' words
total = 0
a = banks['Bank Name'].unique()
for i in a:
    if 'Bank' not in i:
        total+=1
print(total)


# In[193]:


#Count the Bank Name that started with 'S'
total = 0
a = banks['Bank Name'].to_numpy()
for i in a:
    y = i.split()
    z = y[0].split()
    if z[0].startswith('S'):
        total+=1
print(total)


# In[186]:


#Count CERT that above 20000
banks['CERT'][banks['CERT']>20000].count()


# In[192]:


#Count the Bank Name that consist of two words
total = 0
a = banks['Bank Name'].to_numpy()
for i in a:
    z = i.split()
    if len(z) == 2:
        total+=1   
print(total)


# In[201]:


#Show Bank that Closed on 2008
banks['Closing Date'] = pd.to_datetime(banks['Closing Date'])
banks['Bank Name'].loc[(banks['Closing Date']>='01-01-2008')&(banks['Closing Date']<='31-12-2008')].count()

